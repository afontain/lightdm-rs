// This file was generated by gir (https://github.com/gtk-rs/gir @ 2abca11)
// from gir-files (https://github.com/gtk-rs/gir-files @ b89185a)
// DO NOT EDIT

#![allow(non_camel_case_types, non_upper_case_globals, non_snake_case)]
#![allow(
    clippy::approx_constant,
    clippy::type_complexity,
    clippy::unreadable_literal
)]

extern crate gio_sys as gio;
extern crate glib_sys as glib;
extern crate gobject_sys as gobject;
extern crate libc;

#[allow(unused_imports)]
use libc::{
    c_char, c_double, c_float, c_int, c_long, c_short, c_uchar, c_uint, c_ulong, c_ushort, c_void,
    intptr_t, size_t, ssize_t, time_t, uintptr_t, FILE,
};

#[allow(unused_imports)]
use glib::{gboolean, gconstpointer, gpointer, GType};

// Enums
pub type LightDMGreeterError = c_int;
pub const LIGHTDM_GREETER_ERROR_COMMUNICATION_ERROR: LightDMGreeterError = 0;
pub const LIGHTDM_GREETER_ERROR_CONNECTION_FAILED: LightDMGreeterError = 1;
pub const LIGHTDM_GREETER_ERROR_SESSION_FAILED: LightDMGreeterError = 2;
pub const LIGHTDM_GREETER_ERROR_NO_AUTOLOGIN: LightDMGreeterError = 3;
pub const LIGHTDM_GREETER_ERROR_INVALID_USER: LightDMGreeterError = 4;

pub type LightDMMessageType = c_int;
pub const LIGHTDM_MESSAGE_TYPE_INFO: LightDMMessageType = 0;
pub const LIGHTDM_MESSAGE_TYPE_ERROR: LightDMMessageType = 1;

pub type LightDMPromptType = c_int;
pub const LIGHTDM_PROMPT_TYPE_QUESTION: LightDMPromptType = 0;
pub const LIGHTDM_PROMPT_TYPE_SECRET: LightDMPromptType = 1;

// Constants
pub const LIGHTDM_GREETER_SIGNAL_AUTHENTICATION_COMPLETE: *const c_char =
    b"authentication-complete\0" as *const u8 as *const c_char;
pub const LIGHTDM_GREETER_SIGNAL_AUTOLOGIN_TIMER_EXPIRED: *const c_char =
    b"autologin-timer-expired\0" as *const u8 as *const c_char;
pub const LIGHTDM_GREETER_SIGNAL_IDLE: *const c_char = b"idle\0" as *const u8 as *const c_char;
pub const LIGHTDM_GREETER_SIGNAL_RESET: *const c_char = b"reset\0" as *const u8 as *const c_char;
pub const LIGHTDM_GREETER_SIGNAL_SHOW_MESSAGE: *const c_char =
    b"show-message\0" as *const u8 as *const c_char;
pub const LIGHTDM_GREETER_SIGNAL_SHOW_PROMPT: *const c_char =
    b"show-prompt\0" as *const u8 as *const c_char;
pub const LIGHTDM_SIGNAL_USER_CHANGED: *const c_char = b"changed\0" as *const u8 as *const c_char;
pub const LIGHTDM_USER_LIST_SIGNAL_USER_ADDED: *const c_char =
    b"user-added\0" as *const u8 as *const c_char;
pub const LIGHTDM_USER_LIST_SIGNAL_USER_CHANGED: *const c_char =
    b"user-changed\0" as *const u8 as *const c_char;
pub const LIGHTDM_USER_LIST_SIGNAL_USER_REMOVED: *const c_char =
    b"user-removed\0" as *const u8 as *const c_char;

// Records
#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMGreeterClass {
    pub parent_class: gobject::GObjectClass,
    pub show_message:
        Option<unsafe extern "C" fn(*mut LightDMGreeter, *const c_char, LightDMMessageType)>,
    pub show_prompt:
        Option<unsafe extern "C" fn(*mut LightDMGreeter, *const c_char, LightDMPromptType)>,
    pub authentication_complete: Option<unsafe extern "C" fn(*mut LightDMGreeter)>,
    pub autologin_timer_expired: Option<unsafe extern "C" fn(*mut LightDMGreeter)>,
    pub idle: Option<unsafe extern "C" fn(*mut LightDMGreeter)>,
    pub reset: Option<unsafe extern "C" fn(*mut LightDMGreeter)>,
    pub reserved1: Option<unsafe extern "C" fn()>,
    pub reserved2: Option<unsafe extern "C" fn()>,
    pub reserved3: Option<unsafe extern "C" fn()>,
    pub reserved4: Option<unsafe extern "C" fn()>,
}

impl ::std::fmt::Debug for LightDMGreeterClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMGreeterClass @ {:?}", self as *const _))
            .field("show_message", &self.show_message)
            .field("show_prompt", &self.show_prompt)
            .field("authentication_complete", &self.authentication_complete)
            .field("autologin_timer_expired", &self.autologin_timer_expired)
            .field("idle", &self.idle)
            .field("reset", &self.reset)
            .field("reserved1", &self.reserved1)
            .field("reserved2", &self.reserved2)
            .field("reserved3", &self.reserved3)
            .field("reserved4", &self.reserved4)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMLanguageClass {
    pub parent_class: gobject::GObjectClass,
    pub reserved1: Option<unsafe extern "C" fn()>,
    pub reserved2: Option<unsafe extern "C" fn()>,
    pub reserved3: Option<unsafe extern "C" fn()>,
    pub reserved4: Option<unsafe extern "C" fn()>,
    pub reserved5: Option<unsafe extern "C" fn()>,
    pub reserved6: Option<unsafe extern "C" fn()>,
}

impl ::std::fmt::Debug for LightDMLanguageClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMLanguageClass @ {:?}", self as *const _))
            .field("reserved1", &self.reserved1)
            .field("reserved2", &self.reserved2)
            .field("reserved3", &self.reserved3)
            .field("reserved4", &self.reserved4)
            .field("reserved5", &self.reserved5)
            .field("reserved6", &self.reserved6)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMLayoutClass {
    pub parent_class: gobject::GObjectClass,
    pub reserved1: Option<unsafe extern "C" fn()>,
    pub reserved2: Option<unsafe extern "C" fn()>,
    pub reserved3: Option<unsafe extern "C" fn()>,
    pub reserved4: Option<unsafe extern "C" fn()>,
    pub reserved5: Option<unsafe extern "C" fn()>,
    pub reserved6: Option<unsafe extern "C" fn()>,
}

impl ::std::fmt::Debug for LightDMLayoutClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMLayoutClass @ {:?}", self as *const _))
            .field("reserved1", &self.reserved1)
            .field("reserved2", &self.reserved2)
            .field("reserved3", &self.reserved3)
            .field("reserved4", &self.reserved4)
            .field("reserved5", &self.reserved5)
            .field("reserved6", &self.reserved6)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMSessionClass {
    pub parent_class: gobject::GObjectClass,
    pub reserved1: Option<unsafe extern "C" fn()>,
    pub reserved2: Option<unsafe extern "C" fn()>,
    pub reserved3: Option<unsafe extern "C" fn()>,
    pub reserved4: Option<unsafe extern "C" fn()>,
    pub reserved5: Option<unsafe extern "C" fn()>,
    pub reserved6: Option<unsafe extern "C" fn()>,
}

impl ::std::fmt::Debug for LightDMSessionClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMSessionClass @ {:?}", self as *const _))
            .field("reserved1", &self.reserved1)
            .field("reserved2", &self.reserved2)
            .field("reserved3", &self.reserved3)
            .field("reserved4", &self.reserved4)
            .field("reserved5", &self.reserved5)
            .field("reserved6", &self.reserved6)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMUserClass {
    pub parent_class: gobject::GObjectClass,
    pub changed: Option<unsafe extern "C" fn(*mut LightDMUser)>,
    pub reserved1: Option<unsafe extern "C" fn()>,
    pub reserved2: Option<unsafe extern "C" fn()>,
    pub reserved3: Option<unsafe extern "C" fn()>,
    pub reserved4: Option<unsafe extern "C" fn()>,
    pub reserved5: Option<unsafe extern "C" fn()>,
    pub reserved6: Option<unsafe extern "C" fn()>,
}

impl ::std::fmt::Debug for LightDMUserClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMUserClass @ {:?}", self as *const _))
            .field("changed", &self.changed)
            .field("reserved1", &self.reserved1)
            .field("reserved2", &self.reserved2)
            .field("reserved3", &self.reserved3)
            .field("reserved4", &self.reserved4)
            .field("reserved5", &self.reserved5)
            .field("reserved6", &self.reserved6)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMUserListClass {
    pub parent_class: gobject::GObjectClass,
    pub user_added: Option<unsafe extern "C" fn(*mut LightDMUserList, *mut LightDMUser)>,
    pub user_changed: Option<unsafe extern "C" fn(*mut LightDMUserList, *mut LightDMUser)>,
    pub user_removed: Option<unsafe extern "C" fn(*mut LightDMUserList, *mut LightDMUser)>,
    pub reserved1: Option<unsafe extern "C" fn()>,
    pub reserved2: Option<unsafe extern "C" fn()>,
    pub reserved3: Option<unsafe extern "C" fn()>,
    pub reserved4: Option<unsafe extern "C" fn()>,
    pub reserved5: Option<unsafe extern "C" fn()>,
    pub reserved6: Option<unsafe extern "C" fn()>,
}

impl ::std::fmt::Debug for LightDMUserListClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMUserListClass @ {:?}", self as *const _))
            .field("user_added", &self.user_added)
            .field("user_changed", &self.user_changed)
            .field("user_removed", &self.user_removed)
            .field("reserved1", &self.reserved1)
            .field("reserved2", &self.reserved2)
            .field("reserved3", &self.reserved3)
            .field("reserved4", &self.reserved4)
            .field("reserved5", &self.reserved5)
            .field("reserved6", &self.reserved6)
            .finish()
    }
}

// Classes
#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMGreeter {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for LightDMGreeter {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMGreeter @ {:?}", self as *const _))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMLanguage {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for LightDMLanguage {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMLanguage @ {:?}", self as *const _))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMLayout {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for LightDMLayout {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMLayout @ {:?}", self as *const _))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMSession {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for LightDMSession {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMSession @ {:?}", self as *const _))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMUser {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for LightDMUser {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMUser @ {:?}", self as *const _))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct LightDMUserList {
    pub parent_instance: gobject::GObject,
}

impl ::std::fmt::Debug for LightDMUserList {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("LightDMUserList @ {:?}", self as *const _))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

extern "C" {

    //=========================================================================
    // LightDMGreeterError
    //=========================================================================
    pub fn lightdm_greeter_error_get_type() -> GType;
    pub fn lightdm_greeter_error_quark() -> glib::GQuark;

    //=========================================================================
    // LightDMMessageType
    //=========================================================================
    pub fn lightdm_message_type_get_type() -> GType;

    //=========================================================================
    // LightDMPromptType
    //=========================================================================
    pub fn lightdm_prompt_type_get_type() -> GType;

    //=========================================================================
    // LightDMGreeter
    //=========================================================================
    pub fn lightdm_greeter_get_type() -> GType;
    pub fn lightdm_greeter_new() -> *mut LightDMGreeter;
    pub fn lightdm_greeter_authenticate(
        greeter: *mut LightDMGreeter,
        username: *const c_char,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_authenticate_as_guest(
        greeter: *mut LightDMGreeter,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_authenticate_autologin(
        greeter: *mut LightDMGreeter,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_authenticate_remote(
        greeter: *mut LightDMGreeter,
        session: *const c_char,
        username: *const c_char,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_cancel_authentication(
        greeter: *mut LightDMGreeter,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_cancel_autologin(greeter: *mut LightDMGreeter);
    pub fn lightdm_greeter_connect_sync(
        greeter: *mut LightDMGreeter,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_connect_to_daemon(
        greeter: *mut LightDMGreeter,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );
    pub fn lightdm_greeter_connect_to_daemon_finish(
        greeter: *mut LightDMGreeter,
        result: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_connect_to_daemon_sync(
        greeter: *mut LightDMGreeter,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_ensure_shared_data_dir(
        greeter: *mut LightDMGreeter,
        username: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );
    pub fn lightdm_greeter_ensure_shared_data_dir_finish(
        greeter: *mut LightDMGreeter,
        result: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> *mut c_char;
    pub fn lightdm_greeter_ensure_shared_data_dir_sync(
        greeter: *mut LightDMGreeter,
        username: *const c_char,
        error: *mut *mut glib::GError,
    ) -> *mut c_char;
    pub fn lightdm_greeter_get_authentication_user(greeter: *mut LightDMGreeter) -> *const c_char;
    pub fn lightdm_greeter_get_autologin_guest_hint(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_autologin_session_hint(
        greeter: *mut LightDMGreeter,
    ) -> *const c_char;
    pub fn lightdm_greeter_get_autologin_timeout_hint(greeter: *mut LightDMGreeter) -> c_int;
    pub fn lightdm_greeter_get_autologin_user_hint(greeter: *mut LightDMGreeter) -> *const c_char;
    pub fn lightdm_greeter_get_default_session_hint(greeter: *mut LightDMGreeter) -> *const c_char;
    pub fn lightdm_greeter_get_has_guest_account_hint(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_hide_users_hint(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_hint(
        greeter: *mut LightDMGreeter,
        name: *const c_char,
    ) -> *const c_char;
    pub fn lightdm_greeter_get_in_authentication(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_is_authenticated(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_lock_hint(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_select_guest_hint(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_select_user_hint(greeter: *mut LightDMGreeter) -> *const c_char;
    pub fn lightdm_greeter_get_show_manual_login_hint(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_get_show_remote_login_hint(greeter: *mut LightDMGreeter) -> gboolean;
    pub fn lightdm_greeter_respond(
        greeter: *mut LightDMGreeter,
        response: *const c_char,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_set_language(
        greeter: *mut LightDMGreeter,
        language: *const c_char,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_set_resettable(greeter: *mut LightDMGreeter, resettable: gboolean);
    pub fn lightdm_greeter_start_session(
        greeter: *mut LightDMGreeter,
        session: *const c_char,
        cancellable: *mut gio::GCancellable,
        callback: gio::GAsyncReadyCallback,
        user_data: gpointer,
    );
    pub fn lightdm_greeter_start_session_finish(
        greeter: *mut LightDMGreeter,
        result: *mut gio::GAsyncResult,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn lightdm_greeter_start_session_sync(
        greeter: *mut LightDMGreeter,
        session: *const c_char,
        error: *mut *mut glib::GError,
    ) -> gboolean;

    //=========================================================================
    // LightDMLanguage
    //=========================================================================
    pub fn lightdm_language_get_type() -> GType;
    pub fn lightdm_language_get_code(language: *mut LightDMLanguage) -> *const c_char;
    pub fn lightdm_language_get_name(language: *mut LightDMLanguage) -> *const c_char;
    pub fn lightdm_language_get_territory(language: *mut LightDMLanguage) -> *const c_char;
    pub fn lightdm_language_matches(
        language: *mut LightDMLanguage,
        code: *const c_char,
    ) -> gboolean;

    //=========================================================================
    // LightDMLayout
    //=========================================================================
    pub fn lightdm_layout_get_type() -> GType;
    pub fn lightdm_layout_get_description(layout: *mut LightDMLayout) -> *const c_char;
    pub fn lightdm_layout_get_name(layout: *mut LightDMLayout) -> *const c_char;
    pub fn lightdm_layout_get_short_description(layout: *mut LightDMLayout) -> *const c_char;

    //=========================================================================
    // LightDMSession
    //=========================================================================
    pub fn lightdm_session_get_type() -> GType;
    pub fn lightdm_session_get_comment(session: *mut LightDMSession) -> *const c_char;
    pub fn lightdm_session_get_key(session: *mut LightDMSession) -> *const c_char;
    pub fn lightdm_session_get_name(session: *mut LightDMSession) -> *const c_char;
    pub fn lightdm_session_get_session_type(session: *mut LightDMSession) -> *const c_char;

    //=========================================================================
    // LightDMUser
    //=========================================================================
    pub fn lightdm_user_get_type() -> GType;
    pub fn lightdm_user_get_background(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_display_name(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_has_messages(user: *mut LightDMUser) -> gboolean;
    pub fn lightdm_user_get_home_directory(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_image(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_is_locked(user: *mut LightDMUser) -> gboolean;
    pub fn lightdm_user_get_language(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_layout(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_layouts(user: *mut LightDMUser) -> *const *const c_char;
    pub fn lightdm_user_get_logged_in(user: *mut LightDMUser) -> gboolean;
    pub fn lightdm_user_get_name(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_real_name(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_session(user: *mut LightDMUser) -> *const c_char;
    pub fn lightdm_user_get_uid(user: *mut LightDMUser) -> c_uint;

    //=========================================================================
    // LightDMUserList
    //=========================================================================
    pub fn lightdm_user_list_get_type() -> GType;
    pub fn lightdm_user_list_get_instance() -> *mut LightDMUserList;
    pub fn lightdm_user_list_get_length(user_list: *mut LightDMUserList) -> c_int;
    pub fn lightdm_user_list_get_user_by_name(
        user_list: *mut LightDMUserList,
        username: *const c_char,
    ) -> *mut LightDMUser;
    pub fn lightdm_user_list_get_users(user_list: *mut LightDMUserList) -> *mut glib::GList;

    //=========================================================================
    // Other functions
    //=========================================================================
    pub fn lightdm_get_can_hibernate() -> gboolean;
    pub fn lightdm_get_can_restart() -> gboolean;
    pub fn lightdm_get_can_shutdown() -> gboolean;
    pub fn lightdm_get_can_suspend() -> gboolean;
    pub fn lightdm_get_hostname() -> *const c_char;
    pub fn lightdm_get_language() -> *mut LightDMLanguage;
    pub fn lightdm_get_languages() -> *mut glib::GList;
    pub fn lightdm_get_layout() -> *mut LightDMLayout;
    pub fn lightdm_get_layouts() -> *mut glib::GList;
    pub fn lightdm_get_motd() -> *mut c_char;
    pub fn lightdm_get_os_id() -> *const c_char;
    pub fn lightdm_get_os_name() -> *const c_char;
    pub fn lightdm_get_os_pretty_name() -> *const c_char;
    pub fn lightdm_get_os_version() -> *const c_char;
    pub fn lightdm_get_os_version_id() -> *const c_char;
    pub fn lightdm_get_remote_sessions() -> *mut glib::GList;
    pub fn lightdm_get_sessions() -> *mut glib::GList;
    pub fn lightdm_hibernate(error: *mut *mut glib::GError) -> gboolean;
    pub fn lightdm_restart(error: *mut *mut glib::GError) -> gboolean;
    pub fn lightdm_set_layout(layout: *mut LightDMLayout);
    pub fn lightdm_shutdown(error: *mut *mut glib::GError) -> gboolean;
    pub fn lightdm_suspend(error: *mut *mut glib::GError) -> gboolean;

}
