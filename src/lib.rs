#[macro_use]
extern crate glib;
extern crate gio;
extern crate gio_sys;
extern crate glib_sys;
extern crate gobject_sys;
extern crate light_dm_sys;

extern crate libc;

pub use auto::*;

mod auto;
